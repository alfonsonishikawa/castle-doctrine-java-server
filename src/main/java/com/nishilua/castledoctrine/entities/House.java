package com.nishilua.castledoctrine.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nishilua.castledoctrine.auth.User;

@Entity
@Table(name="houses", uniqueConstraints= {@UniqueConstraint(columnNames= {"id"})})
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id", scope=User.class)
public class House implements Serializable {

	private static final long serialVersionUID = 153521295602577463L;

	@Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
	
	@NotNull
	@ManyToOne
	private User user;
	
	@NotNull
	private String houseMapHash ;
	
	// TODO: Set this values into User
	@NotNull
	private String vaultContents ;
	
	@NotNull
	private String backpackContents ;
	
	@NotNull
	private String galleryContents;
	
	@NotNull
	private Integer lastAuctionPrice;
	
	@NotNull
	private Integer musicSeed;
	
	/** Times edited since last successful robbery
     * = 0 if never edited (and not robbable at all)
     * > 0 if successfully edited and robbable
     * < 0 if successfully robbed at least once and still robbable
     */
	@NotNull
	private Integer editCount ;
	
	@NotNull
	private String selfTestHouseMapHash ;
	
	@NotNull
	private Integer lootValue ;
	
	/** Portion of money held by wife */
	@NotNull
	private Integer wifeLotValue ;
	
	/** Loot plus resell value of vault items, rounded */
	@NotNull
	private Integer valueEstimate ;
	
	@NotNull
	private Boolean wifePresent ;
	
	@NotNull
	private Boolean editCheckout ;
	
	@NotNull
	private Boolean selfTestRunning ;
	
	// ignored if not checked out for robbery
    private Integer robbingUserId ;
    
    @NotNull
    private Integer robAttempts ;
    
    @NotNull
    private Integer robberDeaths ;
	
	/** used to count consecutive vault reaches
     * now counts total vault reaches
     * vault pay stops as soon as two vault reaches happen
     */
    @NotNull
    private Integer consecutiveRobSuccessCount ;
    
    @NotNull
    private Integer creationTime ;
    
    @NotNull
    private Integer lastPingTime ;
    
    @NotNull
    private Integer lastOwnerActionTime ;
    
    @NotNull
    private Integer lastOwnerVisitTime ;
    
    @NotNull
    private Integer lastPayCheckTime ;
    
    @NotNull
    private Integer paymentCount ;
    
    @NotNull
    private Integer youPaidTotal ;
    
    @NotNull
    private Integer WifePaidTotal ;
    
    @NotNull
    private Integer bounty ;
    
    @NotNull
    private Integer blocked ;

	public Integer getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public String getHouseMapHash() {
		return houseMapHash;
	}

	public String getVaultContents() {
		return vaultContents;
	}

	public String getBackpackContents() {
		return backpackContents;
	}

	public String getGalleryContents() {
		return galleryContents;
	}

	public Integer getLastAuctionPrice() {
		return lastAuctionPrice;
	}

	public Integer getMusicSeed() {
		return musicSeed;
	}

	public Integer getEditCount() {
		return editCount;
	}

	public String getSelfTestHouseMapHash() {
		return selfTestHouseMapHash;
	}

	public Integer getLootValue() {
		return lootValue;
	}

	public Integer getWifeLotValue() {
		return wifeLotValue;
	}

	public Integer getValueEstimate() {
		return valueEstimate;
	}

	public Boolean getWifePresent() {
		return wifePresent;
	}

	public Boolean getEditCheckout() {
		return editCheckout;
	}

	public Boolean getSelfTestRunning() {
		return selfTestRunning;
	}

	public Integer getRobbingUserId() {
		return robbingUserId;
	}

	public Integer getRobAttempts() {
		return robAttempts;
	}

	public Integer getRobberDeaths() {
		return robberDeaths;
	}

	public Integer getConsecutiveRobSuccessCount() {
		return consecutiveRobSuccessCount;
	}

	public Integer getCreationTime() {
		return creationTime;
	}

	public Integer getLastPingTime() {
		return lastPingTime;
	}

	public Integer getLastOwnerActionTime() {
		return lastOwnerActionTime;
	}

	public Integer getLastOwnerVisitTime() {
		return lastOwnerVisitTime;
	}

	public Integer getLastPayCheckTime() {
		return lastPayCheckTime;
	}

	public Integer getPaymentCount() {
		return paymentCount;
	}

	public Integer getYouPaidTotal() {
		return youPaidTotal;
	}

	public Integer getWifePaidTotal() {
		return WifePaidTotal;
	}

	public Integer getBounty() {
		return bounty;
	}

	public Integer getBlocked() {
		return blocked;
	}

	public House setId(Integer id) {
		this.id = id;
		return this ;
	}

	public House setUser(User user) {
		this.user = user;
		return this ;
	}

	public House setHouseMapHash(String houseMapHash) {
		this.houseMapHash = houseMapHash;
		return this ;
	}

	public House setVaultContents(String vaultContents) {
		this.vaultContents = vaultContents;
		return this ;
	}

	public House setBackpackContents(String backpackContents) {
		this.backpackContents = backpackContents;
		return this ;
	}

	public House setGalleryContents(String galleryContents) {
		this.galleryContents = galleryContents;
		return this ;
	}

	public House setLastAuctionPrice(Integer lastAuctionPrice) {
		this.lastAuctionPrice = lastAuctionPrice;
		return this ;
	}

	public House setMusicSeed(Integer musicSeed) {
		this.musicSeed = musicSeed;
		return this ;
	}

	public House setEditCount(Integer editCount) {
		this.editCount = editCount;
		return this ;
	}

	public House setSelfTestHouseMapHash(String selfTestHouseMapHash) {
		this.selfTestHouseMapHash = selfTestHouseMapHash;
		return this ;
	}

	public House setLootValue(Integer lootValue) {
		this.lootValue = lootValue;
		return this ;
	}

	public House setWifeLotValue(Integer wifeLotValue) {
		this.wifeLotValue = wifeLotValue;
		return this ;
	}

	public House setValueEstimate(Integer valueEstimate) {
		this.valueEstimate = valueEstimate;
		return this ;
	}

	public House setWifePresent(Boolean wifePresent) {
		this.wifePresent = wifePresent;
		return this ;
	}

	public House setEditCheckout(Boolean editCheckout) {
		this.editCheckout = editCheckout;
		return this ;
	}

	public House setSelfTestRunning(Boolean selfTestRunning) {
		this.selfTestRunning = selfTestRunning;
		return this ;
	}

	public House setRobbingUserId(Integer robbingUserId) {
		this.robbingUserId = robbingUserId;
		return this ;
	}

	public House setRobAttempts(Integer robAttempts) {
		this.robAttempts = robAttempts;
		return this ;
	}

	public House setRobberDeaths(Integer robberDeaths) {
		this.robberDeaths = robberDeaths;
		return this ;
	}

	public House setConsecutiveRobSuccessCount(Integer consecutiveRobSuccessCount) {
		this.consecutiveRobSuccessCount = consecutiveRobSuccessCount;
		return this ;
	}

	public House setCreationTime(Integer creationTime) {
		this.creationTime = creationTime;
		return this ;
	}

	public House setLastPingTime(Integer lastPingTime) {
		this.lastPingTime = lastPingTime;
		return this ;
	}

	public House setLastOwnerActionTime(Integer lastOwnerActionTime) {
		this.lastOwnerActionTime = lastOwnerActionTime;
		return this ;
	}

	public House setLastOwnerVisitTime(Integer lastOwnerVisitTime) {
		this.lastOwnerVisitTime = lastOwnerVisitTime;
		return this ;
	}

	public House setLastPayCheckTime(Integer lastPayCheckTime) {
		this.lastPayCheckTime = lastPayCheckTime;
		return this ;
	}

	public House setPaymentCount(Integer paymentCount) {
		this.paymentCount = paymentCount;
		return this ;
	}

	public House setYouPaidTotal(Integer youPaidTotal) {
		this.youPaidTotal = youPaidTotal;
		return this ;
	}

	public House setWifePaidTotal(Integer wifePaidTotal) {
		WifePaidTotal = wifePaidTotal;
		return this ;
	}

	public House setBounty(Integer bounty) {
		this.bounty = bounty;
		return this ;
	}

	public House setBlocked(Integer blocked) {
		this.blocked = blocked;
		return this ;
	}
	
}
