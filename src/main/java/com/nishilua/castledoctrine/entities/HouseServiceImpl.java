/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.castledoctrine.entities;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.nishilua.castledoctrine.auth.User;
import com.nishilua.castledoctrine.auth.UserRepository;

@Service("houseService")
public class HouseServiceImpl implements HouseService {

    @PersistenceContext
    private EntityManager em ;
    
    @Autowired
    private HouseRepository houseRepository ;

	@Override
	public Collection<House> getHouseList(User userListing, Integer skip, Integer limit) {
		// TODO WORK IN PROGRESS
		return null;
	}
    
}
