/*******************************************************************************
 * Copyright 2018 Alfonso Nishikawa
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package com.nishilua.castledoctrine.auth;

import java.util.Collection;

import javax.persistence.EntityNotFoundException;

public interface UserService {

    public User get(String username) ;
    public Collection<User> findAll() ;
    public User findByEmail(String email) ;

    /**
     * @param username
     * @throws IllegalArgumentException if the entity's primary key is null
     * @return
     */
    public Boolean exists(Integer id) ;
    
    /**
     * @param username
     * @throws IllegalArgumentException if the entity's primary key is null
     * @throws EntityNotFoundException if the entity does not exists
     * @return
     */
    public User getReference(Integer id) ;
    public User save(User user) ;
    public void delete(User user) ;
    
    /**
     * Saves a new user. The password must be an unecrypted version.
     * 
     * @throws UserExistsException If the user already exists
     * @param user - The new user to be created
     * @return The saved user, but with the password encrypted
     */
    public User saveNewUser(User user) ;
    
    /**
     * Retrieves the main data of a user:
     *  - User info
     *  - Connections
     *  - Tables
     * @param username
     * @return User
     */
    public User getMainData(String username) ;
    
}
