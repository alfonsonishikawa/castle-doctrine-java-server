create table server_globals (
	last_flush_time integer primary key not null
) WITHOUT ROWID ;

-- A cache of maps, indexed by sha1 hashes of maps
create table maps (
	house_map_hash char(40) primary key not null,
	last_touch_date integer not null,
	delete_flag boolean not null,
	house_map text not null
) WITHOUT ROWID ;

-- This table contains general info about each user's house
-- EVERY user has EXACTLY ONE house
create table houses (
	id integer not null primary key,
	user_id integer not null,
	house_map_hash char(40) not null,
	vault_contents text not null,
	backpack_contents text not null,
	gallery_contents text not null,
	last_auction_price integer not null,
	music_seed integer not null,
	-- times edited since last successful robbery
    -- = 0 if never edited (and not robbable at all)
    -- > 0 if successfully edited and robbable
    -- < 0 if successfully robbed at least once and still robbable
	edit_count integer not null,
	self_test_house_map_hash char(40) not null,
	loot_value integer not null,
	-- portion of money held by wife
	wife_lot_value integer not null,
    -- loot plus resale value of vault items, rounded
    value_estimate integer not null,
    wife_present boolean not null,
    edit_checkout boolean not null,
    self_test_running boolean not null,
    --ignored if not checked out for robbery
    robbing_user_id integer,
    rob_attempts integer not null,
    robber_deaths integer not null,
    -- used to count consecutive vault reaches
    -- now counts total vault reaches
    -- vault pay stops as soon as two vault reaches happen
    consecutive_rob_success_count integer not null,
    creation_time integer not null,
    last_ping_time integer not null,
    last_owner_action_time integer not null,
    last_owner_visit_time integer not null,
    last_pay_check_time integer not null,
    payment_count integer not null,
    you_paid_total integer not null,
    wife_paid_total integer not null,
    bouty integer not null, -- misspelled
    blocked boolean not null,
   	constraint fk_houses_user foreign key(user_id) references users(id)
   	constraint fk_houses_robbing_user foreign key(robbing_user_id) references users(id)
) WITHOUT ROWID ;

-- This maps a user ID to another user ID, where the second
-- ID specifies a house that the first user is ignoring.
-- Forced ignore status cannot be cleared by user and survives
-- if the target house changes or if the owner of the target house
-- starts a new life (ignore status carries over to their new life,
-- until forced_end_time is reached).
create table ignore_houses (
	id integer primary key not null,
	user_id integer not null,
	house_user_id integer not null,
	started boolean not null,
	forced boolean not null,
	forced_pending boolean not null,
	forced_start_time integer not  null
) WITHOUT ROWID ;

-- This maps a user ID to another user ID, where the second
-- ID specifies a house that the first user has developed a chill for.
create table chilling_houses (
	id integer primary key not null,
	user_id integer not null,
	house_user_id integer not null,
	chill_start_time integer not null,
	chill boolean not null
) WITHOUT ROWID ;

-- An entry here for each player that has reached a given player's
-- vault (in their current lives only)
-- Used for computing vault-reach bounties (bounty only goes up
-- the first time a player reaches a given vault in a given time
-- period to prevent bounty inflation).
-- 
-- last_bounty_time tracks the last time a bounty was paid
-- for a vault reach.
-- We can start paying bounties again for vault reach after a certain
-- amount of time has passed (thus blocking the bounty-inflation
-- exploit while still increasing bounties for most real theft).
create table vault_been_reached (
	id integer primary key not null,
	user_id integer not null,
	house_user_id integer not null,
	last_bounty_time integert not null
) WITHOUT ROWID ;

-- contains move log for each robbery
create table robbery_logs (
	id integer primary key not null,
	log_watched boolean not null,
	user_id integer not null,
	house_user_id integer not null,
	loot_value integer not null,
	wife_money integer not null,
	-- if robber died in house,
    -- value_estimate is bounty paid to house owner
    value_estimate integer not null,
    robber_died boolean not null,
    vault_contents text not null,
    gallery_contents text not null,
    music_seed integer not null,
    rob_attempts integer not null,
    robber_deaths integer not null,
    robber_name varchar(62) not null,
    victim_name varchar(62) not null,
    wife_name varchar(62) not null,
    son_name varchar(62) not null,
    daughter_name varchar(62) not null,
    -- flag logs for which the owner is now dead (moved onto a new
    -- character/life) and can no longer see the log
    -- These area candidates for deletion after enough time has passed
    -- (admin should still have access to them for a while).
    owner_now_dead boolean not null,
    rob_time integer not null,
    scouting_count integer not null,
    last_scout_time integer not null,
    house_start_map_hash char(40) not null,
    loadout text not null,
    move_list text not null,
    num_moves integer not null
) WITHOUT ROWID ;

create table prices (
	object_id integer primary key not null,
	price integer not null,
	in_gallery boolean not null,
	order_number integer not null,
	note text not null
) WITHOUT ROWID ;

create table auction (
	object_id integer primary key not null,
	order_number integer not null,
	start_price integer not null,
	start_time integer not null
) WITHOUT ROWID ;
