# Castle Doctrine Java Server

This is a Java reimplementation for The Castle Doctrine v35 game server, originaly created by Jason Rohrer.

You can buy the client at http://thecastledoctrine.net/

LICENSE
=========

All the source code and resources are released under Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License.

CONTRIBUTORS
==============

* Ivan Kurolv